import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BpmContComponent } from './bpm-cont.component';

describe('BpmContComponent', () => {
  let component: BpmContComponent;
  let fixture: ComponentFixture<BpmContComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BpmContComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BpmContComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
