import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-bpm-cont',
  templateUrl: './bpm-cont.component.html',
  styleUrls: ['./bpm-cont.component.css']
})
export class BpmContComponent implements OnInit {

  constructor() { }

  ngOnInit() {
      window.history.pushState("object or string", "Title", "/GSCPBPMWeb/#/bpm");
  }

}
