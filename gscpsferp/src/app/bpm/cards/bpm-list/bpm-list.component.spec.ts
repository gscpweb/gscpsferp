import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BpmListComponent } from './bpm-list.component';

describe('BpmListComponent', () => {
  let component: BpmListComponent;
  let fixture: ComponentFixture<BpmListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BpmListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BpmListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
