import { Component, OnInit, Input, ViewChild, OnChanges } from '@angular/core';
import { MzModalService } from 'ng2-materialize';
import { BpmServiceService } from '../../shared/services/bpm-service.service';
import { GlobalService } from '../../../shared/services/global.service';
import { bpmTask } from '../../data-objects/dto-tasks';
import { TaskDetails } from '../../data-objects/dto-task-details';
import { taskList } from '../../data-objects/dto-task-list';
import { DatatableComponent } from '@swimlane/ngx-datatable';
import { TranslateService } from 'ng2-translate';
import { MzToastService } from 'ng2-materialize';
@Component({
  selector: 'app-bpm-list',
  templateUrl: './bpm-list.component.html',
  styleUrls: ['./bpm-list.component.css']
})
export class BpmListComponent implements OnInit, OnChanges {
  public inboxTask: bpmTask;
  public task;
  public emergencyPOGroup: any;
  public emergencyPOGroupTemp: any;
  public forecastChangeReqGroup: any;
  public forecastChangeReqGroupTemp: any;
  public pOChangeReqGroup: any;
  public pOChangeReqGroupTemp: any;
  public masterDataGroup: any;
  public masterDataGroupTemp;
  public sOChangeReqGroup;
  public sOChangeReqGroupTemp;
  public showTasks: Boolean = false;
  public selectedTask: bpmTask;
  public selectedField: any;
  public emergencyGroupDetails: any;
  public processType;
  public comments;
  isLoading:Boolean = true;
  @ViewChild('detailsModal') modal;
  @ViewChild(DatatableComponent) table: DatatableComponent;
  constructor(private bpmService: BpmServiceService, private gServ: GlobalService, public MzModalService: MzModalService, public translate: TranslateService, private toastService: MzToastService) {
    translate.addLangs(["en"]);
    translate.setDefaultLang('en');
    let browserLang = translate.getBrowserLang();
    translate.use(browserLang.match(/en/) ? browserLang : 'en');
    
  }

  public modalOptions: Materialize.ModalOptions = {
    dismissible: false, // Modal can be dismissed by clicking outside of the modal
    opacity: .5, // Opacity of modal background
    inDuration: 300, // Transition in duration
    outDuration: 200, // Transition out duration
    startingTop: '100%', // Starting top style attribute
    endingTop: '10%', // Ending top style attribute
    ready: (modal, trigger) => { // Callback for Modal open. Modal and trigger parameters available.
      this.getTaskDetails();
    },
    complete: () => {
      if (this.processType.processType == 'OPEN') {
        var selectedCompanyCode;
        var isInList = this.gServ.getUsrConfig().companies.filter(x => {
          x.companyPk === this.gServ.getUsrConfig().user.selected_company;
          if (this.gServ.getUsrConfig().user.selected_company == x.companyPk) {
            selectedCompanyCode = x.companyCode;
          }
          this.comments = '';
          console.log('closing modal');
        });
        var taskObj = {
          "domainUserId": this.gServ.getUsrConfig().user.username,
          "businessEntity": selectedCompanyCode,
          "bpdName": this.selectedTask.groupName
        }
        this.bpmService.getTasks(taskObj).subscribe(
          data => {
            if (this.selectedTask.groupName == 'Emergency Order') {
              this.emergencyPOGroup = data.emergencyPOGroup.task === null ? [] : data.emergencyPOGroup.task;
              this.emergencyPOGroupTemp = data.emergencyPOGroup.task;
            } else if (this.selectedTask.groupName == 'Forecast Change Request') {
              this.forecastChangeReqGroup = data.forecastChangeReqGroup.task === null ? [] : data.forecastChangeReqGroup.task;
              this.forecastChangeReqGroupTemp = data.forecastChangeReqGroup.task;
            } else if (this.selectedTask.groupName == 'Purchase Order Change Request') {
              this.pOChangeReqGroupTemp = data.pOChangeReqGroup.task;
              this.pOChangeReqGroup = data.pOChangeReqGroup.task === null ? [] : data.pOChangeReqGroup.task;
            } else if (this.selectedTask.groupName == 'Sales Order Change Request') {
              this.sOChangeReqGroupTemp = data.sOChangeReqGroup.task;
              this.sOChangeReqGroup = data.sOChangeReqGroup.task === null ? [] : data.sOChangeReqGroup.task;
            } else if (this.selectedTask.groupName == 'Master Data Change Request') {
              this.masterDataGroupTemp = data.masterDataGroup.task;
              this.masterDataGroup = data.masterDataGroup.task === null ? [] : data.masterDataGroup.task;
            }
          }
        )
      } else {
        console.log('good bye!!!!!')
      }

    } // Callback for Modal close
  };

  getTaskDetails() {
    var detailsObj = {
      "domainUserId": this.gServ.getUsrConfig().user.username,
      "bpdName": this.selectedTask.groupName,
      "taskId": this.selectedField.taskId,
      "businessEntity": this.gServ.getUsrConfig().user.selected_company
    }
    this.bpmService.getTaskDetails(detailsObj).subscribe(data => {
      this.emergencyGroupDetails = data.fields;
      this.processType = data;
      console.log(JSON.stringify(this.emergencyGroupDetails))
      console.log('It Works!!!!!')
    })
  }
  approveTask() {
    var approveObj = {
      "domainUserId": this.gServ.getUsrConfig().user.username,
      "response": {
        "comments": this.comments,
        "taskId": this.selectedField.taskId,
        "processedBy": this.gServ.getUsrConfig().user.username,
        "approved": true
      }
    }
    this.bpmService.completeTask(approveObj).subscribe(data => {
      var selectedCompanyCode;
      var isInList = this.gServ.getUsrConfig().companies.filter(x => {
        x.companyPk === this.gServ.getUsrConfig().user.selected_company;
        if (this.gServ.getUsrConfig().user.selected_company == x.companyPk) {
          selectedCompanyCode = x.companyCode;
          console.log(JSON.stringify(x.companyCode));
        }
      });
      var taskObj = {
        "domainUserId": this.gServ.getUsrConfig().user.username,
        "businessEntity": selectedCompanyCode,
        "bpdName": this.selectedTask.groupName
      }
      this.bpmService.getTasks(taskObj).subscribe(
        data => {
          this.toastService.show('Request Approved Succesfully', 5000, 'green');
          if (this.selectedTask.groupName == 'Emergency Order') {
            this.emergencyPOGroup = data.emergencyPOGroup.task === null ? [] : data.emergencyPOGroup.task;
            this.emergencyPOGroupTemp = data.emergencyPOGroup.task;
          } else if (this.selectedTask.groupName == 'Forecast Change Request') {
            this.forecastChangeReqGroup = data.forecastChangeReqGroup.task === null ? [] : data.forecastChangeReqGroup.task;
            this.forecastChangeReqGroupTemp = data.forecastChangeReqGroup.task;
          } else if (this.selectedTask.groupName == 'Purchase Order Change Request') {
            this.pOChangeReqGroupTemp = data.pOChangeReqGroup.task;
            this.pOChangeReqGroup = data.pOChangeReqGroup.task === null ? [] : data.pOChangeReqGroup.task;
          } else if (this.selectedTask.groupName == 'Sales Order Change Request') {
            this.sOChangeReqGroupTemp = data.sOChangeReqGroup.task;
            this.sOChangeReqGroup = data.sOChangeReqGroup.task === null ? [] : data.sOChangeReqGroup.task;
          } else if (this.selectedTask.groupName == 'Master Data Change Request') {
            this.masterDataGroupTemp = data.masterDataGroup.task;
            this.masterDataGroup = data.masterDataGroup.task === null ? [] : data.masterDataGroup.task;
          }

        }
      )



    })
  }
  rejectTask() {
    var approveObj = {
      "domainUserId": this.gServ.getUsrConfig().user.username,
      "response": {
        "comments": this.comments,
        "taskId": this.selectedField.taskId,
        "processedBy": this.gServ.getUsrConfig().user.username,
        "approved": false
      }
    }
    this.bpmService.completeTask(approveObj).subscribe(data => {
      var selectedCompanyCode;
      var isInList = this.gServ.getUsrConfig().companies.filter(x => {
        x.companyPk === this.gServ.getUsrConfig().user.selected_company;
        if (this.gServ.getUsrConfig().user.selected_company == x.companyPk) {
          selectedCompanyCode = x.companyCode;
          console.log(JSON.stringify(x.companyCode));
        }
      });
      var taskObj = {
        "domainUserId": this.gServ.getUsrConfig().user.username,
        "businessEntity": selectedCompanyCode,
        "bpdName": this.selectedTask.groupName
      }
      this.bpmService.getTasks(taskObj).subscribe(
        data => {
          this.toastService.show('Request Rejected Succesfully', 5000, 'green');
          this.getTask();
          if (this.selectedTask.groupName == 'Emergency Order') {
            this.emergencyPOGroup = data.emergencyPOGroup.task === null ? [] : data.emergencyPOGroup.task;
            this.emergencyPOGroupTemp = data.emergencyPOGroup.task;
          } else if (this.selectedTask.groupName == 'Forecast Change Request') {
            this.forecastChangeReqGroup = data.forecastChangeReqGroup.task === null ? [] : data.forecastChangeReqGroup.task;
            this.forecastChangeReqGroupTemp = data.forecastChangeReqGroup.task;
          } else if (this.selectedTask.groupName == 'Purchase Order Change Request') {
            this.pOChangeReqGroupTemp = data.pOChangeReqGroup.task;
            this.pOChangeReqGroup = data.pOChangeReqGroup.task === null ? [] : data.pOChangeReqGroup.task;
          } else if (this.selectedTask.groupName == 'Sales Order Change Request') {
            this.sOChangeReqGroupTemp = data.sOChangeReqGroup.task;
            this.sOChangeReqGroup = data.sOChangeReqGroup.task === null ? [] : data.sOChangeReqGroup.task;
          } else if (this.selectedTask.groupName == 'Master Data Change Request') {
            this.masterDataGroupTemp = data.masterDataGroup.task;
            this.masterDataGroup = data.masterDataGroup.task === null ? [] : data.masterDataGroup.task;
          }

        }
      )
    })
  }

  getTask() {
    var selectedCompanyCode;
    var isInList = this.gServ.getUsrConfig().companies.filter(x => {
      x.companyPk === this.gServ.getUsrConfig().user.selected_company;
      if (this.gServ.getUsrConfig().user.selected_company == x.companyPk) {
        selectedCompanyCode = x.companyCode;
        console.log(JSON.stringify(x.companyCode));
      }
    });

    var taskObj = {
      "domainUserId": this.gServ.getUsrConfig().user.username,
      "businessEntity": selectedCompanyCode,
      "bpdName": ""
    }
    this.bpmService.getTasks(taskObj).subscribe(
      data => {
        this.inboxTask = data;
        this.isLoading=false;
      }
    )
  }
  switchPage() {
    this.showTasks = false;
   
    this.getTask();
   

  }
  selectedTsk(task) {
    this.isLoading=true;
    this.selectedTask = task;
    console.log(this.selectedTask);
    this.showTasks = true;
    var selectedCompanyCode;
    var isInList = this.gServ.getUsrConfig().companies.filter(x => {
      x.companyPk === this.gServ.getUsrConfig().user.selected_company;
      if (this.gServ.getUsrConfig().user.selected_company == x.companyPk) {
        selectedCompanyCode = x.companyCode;
        console.log(JSON.stringify(x.companyCode));
      }
    });
    var taskObj = {
      "domainUserId": this.gServ.getUsrConfig().user.username,
      "businessEntity": selectedCompanyCode,
      "bpdName": this.selectedTask.groupName
    }
    this.bpmService.getTaskByID(taskObj).subscribe(
      data => {
        this.isLoading=false;
        if (this.selectedTask.groupName == 'Emergency Order') {
          this.emergencyPOGroup = data.emergencyPOGroup.task === null ? [] : data.emergencyPOGroup.task;
          this.emergencyPOGroupTemp = data.emergencyPOGroup.task;
        } else if (this.selectedTask.groupName == 'Forecast Change Request') {
          this.forecastChangeReqGroup = data.forecastChangeReqGroup.task === null ? [] : data.forecastChangeReqGroup.task;
          this.forecastChangeReqGroupTemp = data.forecastChangeReqGroup.task;
        } else if (this.selectedTask.groupName == 'Purchase Order Change Request') {
          this.pOChangeReqGroupTemp = data.pOChangeReqGroup.task;
          this.pOChangeReqGroup = data.pOChangeReqGroup.task === null ? [] : data.pOChangeReqGroup.task;
        } else if (this.selectedTask.groupName == 'Sales Order Change Request') {
          this.sOChangeReqGroupTemp = data.sOChangeReqGroup.task;
          this.sOChangeReqGroup = data.sOChangeReqGroup.task === null ? [] : data.sOChangeReqGroup.task;
        } else if (this.selectedTask.groupName == 'Master Data Change Request') {
          this.masterDataGroupTemp = data.masterDataGroup.task;
          this.masterDataGroup = data.masterDataGroup.task === null ? [] : data.masterDataGroup.task;
        }
      }
    )
  }
  selectedApproved(task) {
    this.isLoading=true;
    this.selectedTask = task;
    this.showTasks = true;
    var selectedCompanyCode;
    var isInList = this.gServ.getUsrConfig().companies.filter(x => {
      x.companyPk === this.gServ.getUsrConfig().user.selected_company;
      if (this.gServ.getUsrConfig().user.selected_company == x.companyPk) {
        selectedCompanyCode = x.companyCode;
        console.log(JSON.stringify(x.companyCode));
      }
    });
    var taskObj = {
      "domainUserId": this.gServ.getUsrConfig().user.username,
      "businessEntity": selectedCompanyCode,
      "bpdName": this.selectedTask.groupName,
      "approved": true,
    }
    this.bpmService.getApproved(taskObj).subscribe(
      data => {
        this.isLoading=false;
        if (this.selectedTask.groupName == 'Emergency Order') {
          this.emergencyPOGroup = data.emergencyPOGroup.task === null ? [] : data.emergencyPOGroup.task;
          this.emergencyPOGroupTemp = data.emergencyPOGroup.task;
        } else if (this.selectedTask.groupName == 'Forecast Change Request') {
          this.forecastChangeReqGroup = data.forecastChangeReqGroup.task === null ? [] : data.forecastChangeReqGroup.task;
          this.forecastChangeReqGroupTemp = data.forecastChangeReqGroup.task;
        } else if (this.selectedTask.groupName == 'Purchase Order Change Request') {
          this.pOChangeReqGroupTemp = data.pOChangeReqGroup.task;
          this.pOChangeReqGroup = data.pOChangeReqGroup.task === null ? [] : data.pOChangeReqGroup.task;
        } else if (this.selectedTask.groupName == 'Sales Order Change Request') {
          this.sOChangeReqGroupTemp = data.sOChangeReqGroup.task;
          this.sOChangeReqGroup = data.sOChangeReqGroup.task === null ? [] : data.sOChangeReqGroup.task;
        } else if (this.selectedTask.groupName == 'Master Data Change Request') {
          this.masterDataGroupTemp = data.masterDataGroup.task;
          this.masterDataGroup = data.masterDataGroup.task === null ? [] : data.masterDataGroup.task;
        }
      }
    )
  }
  selectedReject(task) {
    this.isLoading=true;
    this.selectedTask = task;
    this.showTasks = true;
    var selectedCompanyCode;
    var isInList = this.gServ.getUsrConfig().companies.filter(x => {
      x.companyPk === this.gServ.getUsrConfig().user.selected_company;
      if (this.gServ.getUsrConfig().user.selected_company == x.companyPk) {
        selectedCompanyCode = x.companyCode;
        console.log(JSON.stringify(x.companyCode));
      }
    });
    var taskObj = {
      "domainUserId": this.gServ.getUsrConfig().user.username,
      "businessEntity": selectedCompanyCode,
      "bpdName": this.selectedTask.groupName,
      "approved": false,
    }
    this.bpmService.getApproved(taskObj).subscribe(
      data => {
        this.isLoading=false;
        if (this.selectedTask.groupName == 'Emergency Order') {
          this.emergencyPOGroup = data.emergencyPOGroup.task === null ? [] : data.emergencyPOGroup.task;
          this.emergencyPOGroupTemp = data.emergencyPOGroup.task;
        } else if (this.selectedTask.groupName == 'Forecast Change Request') {
          this.forecastChangeReqGroup = data.forecastChangeReqGroup.task === null ? [] : data.forecastChangeReqGroup.task;
          this.forecastChangeReqGroupTemp = data.forecastChangeReqGroup.task;
        } else if (this.selectedTask.groupName == 'Purchase Order Change Request') {
          this.pOChangeReqGroupTemp = data.pOChangeReqGroup.task;
          this.pOChangeReqGroup = data.pOChangeReqGroup.task === null ? [] : data.pOChangeReqGroup.task;
        } else if (this.selectedTask.groupName == 'Sales Order Change Request') {
          this.sOChangeReqGroupTemp = data.sOChangeReqGroup.task;
          this.sOChangeReqGroup = data.sOChangeReqGroup.task === null ? [] : data.sOChangeReqGroup.task;
        } else if (this.selectedTask.groupName == 'Master Data Change Request') {
          this.masterDataGroupTemp = data.masterDataGroup.task;
          this.masterDataGroup = data.masterDataGroup.task === null ? [] : data.masterDataGroup.task;
        }
      }
    )
  }
  selectedFld(row) {
    this.selectedField = row;
    this.modal.open();
  }
  filterForecastChange(event) {
    const val = event.target.value.toLowerCase();
    const temp = this.forecastChangeReqGroupTemp.filter(function (d) {
      return d.taskId.toLowerCase().indexOf(val) !== -1 || d.requestedBy.toLowerCase().indexOf(val) !== -1 || d.customerCode.toLowerCase().indexOf(val) !== -1 || d.customerName.toLowerCase().indexOf(val) !== -1 || d.itemCode.toLowerCase().indexOf(val) !== -1 || d.periodName.toLowerCase().indexOf(val) !== -1 || d.requestedDate.toLowerCase().indexOf(val) !== -1 || !val;
    });
    this.forecastChangeReqGroup = temp;
    this.table.offset = 0;
  }
  filterEmergency(event) {
    const val = event.target.value.toLowerCase();
    const temp = this.emergencyPOGroupTemp.filter(function (d) {
      return d.taskId.toLowerCase().indexOf(val) !== -1 || d.requestedBy.toLowerCase().indexOf(val) !== -1 || d.customerCode.toLowerCase().indexOf(val) !== -1 || d.customerName.toLowerCase().indexOf(val) !== -1 || d.itemCode.toLowerCase().indexOf(val) !== -1 || d.pONumber.toLowerCase().indexOf(val) !== -1 || d.requestedDate.toLowerCase().indexOf(val) !== -1 || !val;
    });
    this.emergencyPOGroup = temp;
    this.table.offset = 0;
  }
  filterPOChange(event) {
    const val = event.target.value.toLowerCase();
    const temp = this.pOChangeReqGroupTemp.filter(function (d) {
      return d.taskId.toLowerCase().indexOf(val) !== -1 || d.requestedBy.toLowerCase().indexOf(val) !== -1 || d.customerCode.toLowerCase().indexOf(val) !== -1 || d.customerName.toLowerCase().indexOf(val) !== -1 || d.itemCode.toLowerCase().indexOf(val) !== -1 || d.pONumber.toLowerCase().indexOf(val) !== -1 || d.requestedDate.toLowerCase().indexOf(val) !== -1 || !val;
    });
    this.pOChangeReqGroup = temp;
    this.table.offset = 0;

  }
  filterMaster(event) {
    const val = event.target.value.toLowerCase();
    const temp = this.masterDataGroupTemp.filter(function (d) {
      console.log('Filtering: ', val);
      return d.taskId.toLowerCase().indexOf(''+val) !== -1 || d.requestedBy.toLowerCase().indexOf(''+val) !== -1 || d.masterDataType.toLowerCase().indexOf(val) !== -1 || (''+d.filterValue).toLowerCase().indexOf(''+val) !== -1 || d.requestedDate.toLowerCase().indexOf(''+val) !== -1 || !val;
    });
    
    this.masterDataGroup = temp;
    this.table.offset = 0;
  }
  filterSOChange(event) {
    const val = event.target.value.toLowerCase();
    const temp = this.sOChangeReqGroupTemp.filter(function (d) {
      return d.taskId.toLowerCase().indexOf(val) !== -1 || d.requestedBy.toLowerCase().indexOf(val) !== -1 || d.customerCode.toLowerCase().indexOf(val) !== -1 || d.customerName.toLowerCase().indexOf(val) !== -1 || d.itemCode.toLowerCase().indexOf(val) !== -1 || d.requestedDate.toLowerCase().indexOf(val) !== -1 || !val;
    });
    this.sOChangeReqGroup = temp;
    this.table.offset = 0;
  }
  ngOnInit() {
    this.comments = '';
    this.getTask();
  }
  ngOnChanges() {
    this.getTaskDetails();

  }

}
