import {DtoTask} from './dto-task';
export class masterDataGroup {
     taskId: String;
    assignedTo: String;
    status: String;
    subject: String;
    bpdName: String;
    priority: String;
    claimed: String;
    requestedBy: String;
    requestedDate: String;
    customerCode: String;
    customerName: String;
    itemCode: String;
    pONumber: String;
    task:DtoTask[];
}
