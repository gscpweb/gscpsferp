export class TaskDetails {
    fieldName: String;
    oldValue: String;
    newValue: String;
    changed: Boolean;
}