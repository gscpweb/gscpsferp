import { emergencyPOGroup } from './dto-emergencyPOGroup';
import {forecastChangeReqGroup} from './dto-forecastChangeReqGroup';
import {masterDataGroup } from './dto-master-data-group';
import {pOChangeReqGroup} from './dto-p-ochange-req-group';
import {sOChangeReqGroup} from './dto-s-ochange-req-group';
export class taskList{
    emergencyPOGroup:emergencyPOGroup;
    forecastChangeReqGroup:forecastChangeReqGroup;
    masterDataGroup :masterDataGroup;
    pOChangeReqGroup:pOChangeReqGroup;
    sOChangeReqGroup:sOChangeReqGroup;
}