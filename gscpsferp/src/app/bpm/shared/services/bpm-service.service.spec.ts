import { TestBed, inject } from '@angular/core/testing';

import { BpmServiceService } from './bpm-service.service';

describe('BpmServiceService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [BpmServiceService]
    });
  });

  it('should be created', inject([BpmServiceService], (service: BpmServiceService) => {
    expect(service).toBeTruthy();
  }));
});
