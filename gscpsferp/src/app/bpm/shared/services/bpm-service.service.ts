import { Injectable } from '@angular/core';
import { Http, RequestOptions, Headers } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import {GenericRestService} from '../../../shared/services/generic-rest.service';
import 'rxjs/add/operator/map';
import { MzToastService } from 'ng2-materialize';
import { StaticHost } from '../../../shared/services/static-hosts';

@Injectable()
export class BpmServiceService extends GenericRestService {

  public headers = new Headers({ 'Content-type': 'application/json' });
  public options = new RequestOptions({ headers: this.headers });
  public staticHost: StaticHost = new StaticHost();
  constructor(protected http: Http, protected toastService: MzToastService) 
  {
    super(http, toastService); 
  }

   getTasks(data){
     return this.http.post(this.staticHost.bpmTasksURL, data, this.options).map(
       (res) => 
        this.handleAuth(res)
    ).catch(err => this.handleError(err))
  }
  getTaskByID(data){
    return this.http.post(this.staticHost.bpmTaskById, data, this.options).map(
       (res) => 
        this.handleAuth(res)
    ).catch(err => this.handleError(err))
  }

  getApproved(data){
    return this.http.post(this.staticHost.completedTaskURL, data, this.options).map(
       (res) => 
        this.handleAuth(res)
    ).catch(err => this.handleError(err))
  }
  getTaskDetails(data){
   return this.http.post(this.staticHost.taskDetailsURL, data, this.options).map(
       (res) => 
        this.handleAuth(res)
    ).catch(err => this.handleError(err))
  }
  completeTask(data){
   return this.http.post(this.staticHost.actionURL, data, this.options).map(
       (res) => 
        this.handleAuth(res)
    ).catch(err => this.handleError(err))
  }


}
