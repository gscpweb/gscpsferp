import { Component, OnInit } from '@angular/core';
import { GlobalService } from '../shared/services/global.service';
import { DtoAppConfig } from '../shared/data-objects/dto-app-config';

@Component({
  selector: 'app-core',
  templateUrl: './core.component.html',
  styleUrls: ['./core.component.css']
})
export class CoreComponent implements OnInit {

  constructor(private gserv: GlobalService) { }


  private appConfig: DtoAppConfig;
  private usrConfig: DtoAppConfig;

  ngOnInit() {
    console.log("CoreComponent - but why the const and the privates????")
    const coreAppConfig = this.gserv.getUsrConfig();
  }

}
