import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-disclaimer',
  templateUrl: './disclaimer.component.html',
  styleUrls: ['./disclaimer.component.css']
})
export class DisclaimerComponent implements OnInit {
  private isNotChecked: boolean = false;

  constructor() { }

  setChecked() {
    this.isNotChecked = !this.isNotChecked;
  }
  ngOnInit() {
  }

}
