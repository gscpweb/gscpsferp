import { Component, OnInit } from '@angular/core';
import { GlobalService } from '../../shared/services/global.service';

@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.css']
})
export class FooterComponent implements OnInit {

  public footerCopyright;

  constructor(private gserv: GlobalService) { }

  ngOnInit() {
    this.footerCopyright = this.gserv.getAppConfig().cfgFooter[0].cfgCopyright; 
  }

}
