import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UserprefComponent } from './userpref.component';

describe('UserprefComponent', () => {
  let component: UserprefComponent;
  let fixture: ComponentFixture<UserprefComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UserprefComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UserprefComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
