import { Component, OnInit } from '@angular/core';
import { MzBaseModal } from 'ng2-materialize';
import { AbstractControl, FormArray, FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import { GlobalService } from '../../../../shared/services/global.service';
import { NavbarService } from '../../../../shared/services/navbar/navbar.service';

@Component({
  selector: 'app-userpref',
  templateUrl: './userpref.component.html',
  styleUrls: ['./userpref.component.css']
})

export class UserprefComponent extends MzBaseModal implements OnInit {
  public userInfo: any;
  public entity: any;
  public languge:any;
  public cell: any;
  public  email: any;
  public allCompany:any;
  constructor(private gServ: GlobalService, private navService: NavbarService) {
    super();
  }

  updatePreference() {
    var userObj = {
      "emailAddress": this.email,
      "cellNumber": this.cell,
      "preferredLanguage": this.languge,
      "preferredBusinessEntityId": this.entity,
      "userPK": this.gServ.getUsrConfig().user.userPk
    }
    this.navService.updateUserPref(userObj).subscribe(data=>{
      console.log('hala it works')
    })
    
  }

  ngOnInit() {
    this.email=this.gServ.getUsrConfig().user.emailAddress;
     this.cell=this.gServ.getUsrConfig().user.cellNumber;
     this.languge=this.gServ.getUsrConfig().user.preferredLanguage;
     this.entity=this.gServ.getUsrConfig().user.selected_company;
     this.allCompany=this.gServ.getUsrConfig().companies;
     this.userInfo=this.gServ.getUsrConfig().user;

  }

}
