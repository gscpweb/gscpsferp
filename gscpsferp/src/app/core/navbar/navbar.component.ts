import { Component, OnInit, ViewChild } from '@angular/core';
import { Http, Headers } from '@angular/http';
import { ModalComponent } from 'ng2-bs3-modal/ng2-bs3-modal';
import { MzModalService } from 'ng2-materialize';

import { GlobalService } from '../../shared/services/global.service';
import { NavbarService } from '../../shared/services/navbar/navbar.service';
import { UserprefComponent } from './modal/userpref/userpref.component';

import { Router } from '@angular/router';

import { DtoAppCompany } from '../../shared/data-objects/dto-app-company';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent implements OnInit {

  @ViewChild('modal') modal: ModalComponent;
  public isModal: Boolean = false;
  public businessEntities: DtoAppCompany[];
  public busEntitySelected: any;
  public userPk: String;
  public menuItems;
  public userPref;
  public userDisclaimer: Date;

  // private SELECTED_COOKIE_ID = '_selected';

  public navbarMenu;
  public navBarLogo;

  constructor(
    private navbarService: NavbarService,
    private modalService: MzModalService,
    private gserv: GlobalService,
    private http: Http,
    private router: Router,
  ) { }


  public userModal() {
    this.modalService.open(UserprefComponent, { userPref: this.gserv.getUsrConfig().user });
  }

  onChange(newValue) {
    console.log('Changing entities');
    this.gserv.getUsrConfig().user.selected_company = this.busEntitySelected = newValue;
    // this.router.navigate(['GSCPWeb/index.html#/notifications'], { queryParams: { company: this.busEntitySelected } });
    window.location.href = '/GSCPWeb/index.html#/notifications?company=' + this.busEntitySelected;
  }


  isShowItem(name) {
    for (let pos = 0; pos < this.businessEntities.length; pos++) {
      if (this.busEntitySelected === this.businessEntities[pos].companyPk) {
        if (this.businessEntities[pos].allowedMenu.indexOf(name) > -1) {
          return true;
        } else {
          return false;
        }
      }
    }
    //return true
  }
  getQueryParam() {
    return {
      'company':
      this.busEntitySelected
    }

  }

  getHREFParam(path: string) {
    return path + '?company=' +
      this.busEntitySelected;

  }

  ngOnInit() {

    const uconf = this.gserv.getUsrConfig();
    this.navbarMenu = this.gserv.getAppConfig().cfgNavbarMenu;
    console.log('------------------' + JSON.stringify(this.navbarMenu))
    this.navBarLogo = this.gserv.getAppConfig().cfgNavbarLogo;
    this.businessEntities = this.gserv.getUsrConfig().companies;
    this.busEntitySelected = this.gserv.getUsrConfig().user.selected_company;
    this.userDisclaimer = this.gserv.getUsrConfig().user.disclaimerDate;

  }

}
