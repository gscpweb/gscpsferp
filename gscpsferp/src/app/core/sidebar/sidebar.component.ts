import { Component, OnInit } from '@angular/core';
import { GlobalService } from '../../shared/services/global.service';

@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.css']
})
export class SidebarComponent implements OnInit {

  public sidebarMenu:any;

  constructor( private gserv: GlobalService) { }

  ngOnInit() {
    this.sidebarMenu = this.gserv.getAppConfig().cfgSidebarMenu;
    console.log(JSON.stringify("SidebarComponent -> sidebarMenu -> " + JSON.stringify(this.sidebarMenu)));
  }


}
