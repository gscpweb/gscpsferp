import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SendActionComponent } from './send-action.component';

describe('SendActionComponent', () => {
  let component: SendActionComponent;
  let fixture: ComponentFixture<SendActionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SendActionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SendActionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
