import { Component, OnInit } from '@angular/core';
import {ErpServiceService} from '../../shared/services/erp-service.service';
import { MzToastService } from 'ng2-materialize';
import {GlobalService} from '../../../shared/services/global.service';
import { Cookie } from 'ng2-cookies';
import { CookieService } from 'ngx-cookie-service';

@Component({
  selector: 'app-send-action',
  templateUrl: './send-action.component.html',
  styleUrls: ['./send-action.component.css']
})
export class SendActionComponent implements OnInit {
  disableFlag :boolean;
  myCookie:string='unknwon';
  
  // current time + 6 hours /// expire.setTime(time); Cookie.set("test", "", expire);
  constructor(private erpService:ErpServiceService, private gServ: GlobalService, public toastService: MzToastService,private CookieService:CookieService ) { }
  
  forecastSend(){
    //this.disableFlag=true
    // this.CookieService.delete('gscp_PlanningSend');
     this.CookieService.set('gscp_PlanningSend', 'hide', 0.06);
     //this.myCookie = Cookie.get('cookieName');
    var sendOBJ={}
    // this.erpService.sendToERP(sendOBJ).subscribe(data=>{
    //   Cookie.set('gscp_PlanningSend', 'hide', 0.10);
    //   this.toastService.show('Forecast Sent To ERP Succesfully', 5000, 'green');
     
    // })
  }

  getCookie(key: string){
    return this.CookieService.get(key);
  }

  ngOnInit() {
    this.myCookie = this.CookieService.get('gscp_PlanningSend');

    console.log(this.myCookie);
    var isCookie=this.CookieService.check('gscp_PlanningSend')

    const cookieExists: boolean = this.CookieService.check('gscp_PlanningSend');
    

    console.log(cookieExists);
  
        
    // if(this.myCookie =='hide'){
    //   this.disableFlag=true
    // }else{
    //   this.disableFlag=false
    // }
  }

}
