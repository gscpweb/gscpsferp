import { TestBed, inject } from '@angular/core/testing';

import { ErpServiceService } from './erp-service.service';

describe('ErpServiceService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ErpServiceService]
    });
  });

  it('should be created', inject([ErpServiceService], (service: ErpServiceService) => {
    expect(service).toBeTruthy();
  }));
});
