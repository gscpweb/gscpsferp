export class DtoAppCompany {
    companyCode: string;
    companyName: string;
    companyPk: number;
    allowedMenu: string[];
}
