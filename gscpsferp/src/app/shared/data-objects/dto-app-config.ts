import { DtoAppCompany } from './dto-app-company';
import { DtoAppUser } from './dto-app-user';

export class DtoAppConfig {
    
    user: DtoAppUser;
    companies: DtoAppCompany[];
    detailsPage: boolean;

}
