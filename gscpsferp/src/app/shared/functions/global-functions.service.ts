import { Injectable } from '@angular/core';

@Injectable()
export class GlobalFunctionsService {

  constructor() { }

  locationOpen(url, selectCompany) {
    window.location.href = url + "?" + btoa("company=" + selectCompany);
  }

  locationChange(url, selectCompany) {
    window.location.href = url + "?" + btoa("company=" + selectCompany);
  }

  isLocal() {
    var url = window.location.href;
    if (url.indexOf('localhost') >= 0) {
      return true;
    } else {
      return false;
    }
  }

}