import { TestBed, inject } from '@angular/core/testing';

import { NavbarFunctionsService } from './navbar-functions.service';

describe('NavbarFunctionsService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [NavbarFunctionsService]
    });
  });

  it('should be created', inject([NavbarFunctionsService], (service: NavbarFunctionsService) => {
    expect(service).toBeTruthy();
  }));
});
