import { Injectable } from '@angular/core';
import { GlobalService } from '../services/global.service';
import { DtoAppUser } from '../../shared/data-objects/dto-app-user';

@Injectable()
export class NavbarFunctionsService {

  constructor(private gSrvc: GlobalService) { }

  locationOpen(url, selectCompany) {
    window.location.href = url + "?" + btoa("company=" + selectCompany);
  }

  locationChange(url, selectCompany) {
    window.location.href = url + "?" + btoa("company=" + selectCompany);
  }

  isLocal() {
    var url = window.location.href;
    if (url.indexOf('localhost') >= 0) {
      return true;
    } else {
      return false;
    }
  }

  getGlobalConfig() {
    return this.gSrvc.getAppConfig();
  }

  // setBusinessEntities(busEnt: any[]) {
  //   this.gSrvc.setAppConfigEntities(busEnt);
  // }

  // setUserConfig(user: DtoAppUser) {
  //   this.gSrvc.setAppConfigUser(user);
  // }

  cleanURL() {
    var base = "/GSCBPMWeb/";
    //get full url
    var url = window.location.href;
    if (url.indexOf(base) > 0)
      url = url.substring(0, (url.lastIndexOf(base)));
    return url;
  }

  // http://localhost:4200/?user=300041&company=AGI

//   refineUrl() {

//     var base = "/GSCPWeb/";
//     //get full url
//     var url = window.location.href;

//     var value = url.substring(0, url.lastIndexOf('?'));

//     if (value.indexOf(base) > 0)
//       value = url.substring(0, (url.lastIndexOf(base) + base.length));

//     var returnVal = url.substring(url.lastIndexOf('?') + 1);

//     try {
//       returnVal = atob(returnVal);
//     } catch (e) {
//       console.log("Invalid cypher!");
//     }

//     if (returnVal.indexOf("=") >= 0) {
//       if (returnVal.indexOf("&") < 0) {
//         returnVal = returnVal.split("=")[1];
//       }
//       else {
//         var query = returnVal.split("&");
//         var usrDto = new DtoAppUser;
//         for (var i = 0; i < query.length; i++) {
//           if (query[i].indexOf("company") >= 0) {

//             returnVal = query[i].split("=")[1];
//             usrDto.selected_company = Number(returnVal);
//           }
//           if (query[i].indexOf("userPk") >= 0) {
//             usrDto.userPk = Number(query[i].split("=")[1]);
//           }
//         }
//         this.gSrvc.setAppConfigUser(usrDto);
//       }
//     } else {
//       returnVal = "1";
//     }

//     window.history.pushState("object or string", "Title", value + "#/orderbook");

//     return returnVal;
//   }
}
