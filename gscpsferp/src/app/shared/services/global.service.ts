import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';
import { ConfigService } from './config.service';
import { UserConfigService } from './user-config.service';
import { DtoAppConfig } from '../data-objects/dto-app-config';

@Injectable()
export class GlobalService {

  private appConfig;
  private usrConfig: DtoAppConfig;

  constructor(private http: Http, private cserv: ConfigService, private userv: UserConfigService) { }

  public tglDetailsPage() {
    this.appConfig.detailsPage = !this.appConfig.detailsPage;
    return this.appConfig.detailsPage;
  }

  public getAppConfig() {
    return this.appConfig;
  }

  public getUsrConfig() {
    return this.usrConfig;
  }

  public setAppConfig(): void {
    this.appConfig = this.cserv.config;
    this.usrConfig = this.userv.config;
  }
}
