import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import 'rxjs/add/operator/toPromise';
import { Observable } from 'rxjs/Observable';
import { ActivatedRoute } from '@angular/router';
import {StaticHost} from './static-hosts';
import { DtoAppUser } from '../data-objects/dto-app-user';
import { DtoAppConfig } from '../data-objects/dto-app-config';
import { DtoAppCompany } from '../data-objects/dto-app-company';


@Injectable()
export class UserConfigService {

  private _user_cfg: DtoAppConfig;
  private errorMessage: any = '';
  public host:StaticHost=new StaticHost();
  private _user_details_url = this.host.userURL;

  // private SELECTED_COOKIE_ID = '_selected';
  // private route: ActivatedRoute;

  constructor(
    private http: Http,
  ) {
  }

  // Loads the config at start
  load(): Promise<any> {

    if (this._user_cfg == null) {
      this._user_cfg = new DtoAppConfig;
      this._user_cfg.user = new DtoAppUser;
      this._user_cfg.companies = new Array<DtoAppCompany>();
    }

    this._user_cfg.user.userPk = null;
    this._user_cfg.user.firstname = '~';
    this._user_cfg.user.lastname = '~';
    this._user_cfg.detailsPage = false;
    this._user_cfg.user.isInternal = false;
    // this._user_cfg.user.isCRFApprover = false;
    // this._user_cfg.user.isCRFRequestor = false;

    return this.http.get(this._user_details_url)
      .map((response: Response) => response.json())
      .toPromise()
      .then(data => {
        // Setting the user date if there any.
        this._user_cfg.user = this.setUsrDetails(data.user);

        // if (this.getSelectedBusinessntity() === -1 && data.user.preferredBusinessentity !== undefined) {
        //   if (data.user.preferredBusinessentity !== -1) {
        //     this.setSelectedBusinessntity(data.user.preferredBusinessentity);
        //   } else {
        //     this.setSelectedBusinessntity(data.companies[0].companyPk);
        //   }
        // }

        this._user_cfg.user.selected_company = Number(this.getUserSelectedCompany());
        // this._user_cfg.user.isCRFApprover = false;
        // this._user_cfg.user.isCRFRequestor = false;

        // Setting the busieness entities
        this._user_cfg.companies = this.setUsrCompanies(data.companies);

        if (this._user_cfg.user.selected_company === -1) {
          this._user_cfg.user.selected_company = data.companies[0].companyPk;
        }

        this._user_cfg.user.hasAgi = false;
        let hasOrderbook = false;


        data.companies.forEach(company => {
          if (company.companyPk === 1) {
            this._user_cfg.user.hasAgi = true;
          }

          company.allowedMenu.filter(menu => menu === 'Order Book').forEach(element => {
            hasOrderbook = true;
          });

        });

        data.companies.filter(companies => companies.companyPk === this._user_cfg.user.selected_company).forEach(company => {

          // Internal AGI with CRF Approver role assigned in user management.
          if (this._user_cfg.user.isInternal && this._user_cfg.user.hasAgi && hasOrderbook) {
            company.roles.filter(role => role.roleName === 'CRF Approver').forEach(element => {
              this._user_cfg.user.isCRFApprover = true;
            });
          }

          // Internal with CRF Requester role assigned in user management.
          if (!this._user_cfg.user.isCRFRequestor) {
            company.roles.filter(role => role.roleName === 'CRF Requester').forEach(element => {
              this._user_cfg.user.isCRFRequestor = true;
            });
            // Internal or external user Orderbook (PO) role is a requester.
          }
          if (!this._user_cfg.user.isCRFRequestor) {
            company.allowedMenu.filter(menu => menu === 'Order Book').forEach(element => {
              this._user_cfg.user.isCRFRequestor = true;
            });

            // External user with access to AGI by default a requester.
          }
          if (!this._user_cfg.user.isCRFRequestor) {
            if (this._user_cfg.user.hasAgi && !this._user_cfg.user.isInternal) {
              this._user_cfg.user.isCRFRequestor = true;
            }
            // External user with access to AGI by default a requester.
          }
          if (!this._user_cfg.user.isCRFRequestor) {
            if (!this._user_cfg.user.hasAgi && this._user_cfg.user.isInternal) {
              this._user_cfg.user.isCRFRequestor = true;
            }
          }
          console.log('[object Verify]',
            'Internal', this._user_cfg.user.isInternal,
            'AGI', this._user_cfg.user.hasAgi,
            'Orderbook', hasOrderbook,
            'Requester', this._user_cfg.user.isCRFRequestor,
            'Approver', this._user_cfg.user.isCRFApprover);
        });
        if (this._user_cfg.user.isCRFApprover === undefined) {
          this._user_cfg.user.isCRFApprover = false;
        }
        if (this._user_cfg.user.isCRFRequestor === undefined) {
          this._user_cfg.user.isCRFRequestor = false;
        }
        return data;
      },
      error => {
        this.errorMessage = <any>error;
      }
      ).catch(this.handleError);

  }



  // This will return the config that was loaded.
  get config(): any {
    return this._user_cfg;
  }

  setNotFound() {
    this._user_cfg.user.userPk = 999999999;
    this._user_cfg.user.firstname = 'Not';
    this._user_cfg.user.lastname = 'Found';
  }

  // **********************
  // ** Helper functions **
  // **********************
  private handleError(error: any) {
    const errMsg = (error.message) ? error.message :
      error.status ? `${error.status} - ${error.statusText}` : 'Server error';
    return Observable.throw(errMsg);
  }

  private setUsrDetails(user) {

    const theUsr: DtoAppUser = new DtoAppUser();

    if (user.userPk != null) {
      theUsr.userPk = user.userPk;
      const data = this.http.get((this._user_details_url).replace('user', user.userPk))
        .map((response: Response) => response.json())
        .toPromise()
        .then(udetails => {
          if (udetails.emailAddress != null) { theUsr.emailAddress = udetails.emailAddress; }
          if (udetails.cellNumber != null) { theUsr.cellNumber = udetails.cellNumber; }
          if (udetails.preferredBusinessEntityId != null) { theUsr.preferredBusinessEntityId = udetails.preferredBusinessEntityId; }
          if (udetails.preferredLanguage != null) { theUsr.preferredLanguage = udetails.preferredLanguage; }
        });
    }
    if (user.username != null) { theUsr.username = user.username; }
    if (user.firstname != null) { theUsr.firstname = user.firstname; }
    if (user.lastname != null) { theUsr.lastname = user.lastname; }
    if (user.internal != null) {
      theUsr.isInternal = user.internal;
    }
    return theUsr;
  }

  private setUsrCompanies(companies) {

    const dacl: Array<DtoAppCompany> = new Array<DtoAppCompany>();

    companies.forEach(entity => {

      const dace: DtoAppCompany = new DtoAppCompany();

      dace.companyCode = entity.companyCode;
      dace.companyName = entity.companyName;
      dace.companyPk = entity.companyPk;
      dace.allowedMenu = entity.allowedMenu;

      dacl.push(dace);
    });

    return dacl;
  }

  private getUserSelectedCompany() {

    // let returnVal = '' + this.getSelectedBusinessntity();
    let returnVal = '-1'; // + this.getSelectedBusinessntity();

    if (+returnVal === -1) {
      const base = '/GSCPWeb/';
      // get full url
      const url = window.location.href;

      let value = url.substring(0, url.lastIndexOf('?'));

      if (value.indexOf(base) > 0) {
        value = url.substring(0, (url.lastIndexOf(base) + base.length));
      }

      returnVal = url.substring(url.lastIndexOf('?') + 1);

      // try {
      //   returnVal = atob(returnVal);
      // } catch (e) {
      //   // console.log('Invalid cypher!');
      // }

      if (returnVal.indexOf('=') >= 0) {
        if (returnVal.indexOf('&') < 0) {
          returnVal = returnVal.split('=')[1];
        } else {
          const query = returnVal.split('&');
          const usrDto = new DtoAppUser;
          for (let i = 0; i < query.length; i++) {
            if (query[i].indexOf('company') >= 0) {

              returnVal = query[i].split('=')[1];
              usrDto.selected_company = Number(returnVal);
            }
            if (query[i].indexOf('userPk') >= 0) {
              usrDto.userPk = Number(query[i].split('=')[1]);
            }
          }
        }
      } else {
        returnVal = '-1';
      }
    }
    return returnVal;
  }

}
